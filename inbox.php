<?php 
require 'header.php';
if (!isset($_SESSION['email'])) {
	echo "<script>window.location='login.php';</script>";
}else{
	$email=$_SESSION['email'];
}
 ?>
 <style>
 	.msg-header{
 		padding: 10px;
 		border-bottom: 1px solid lightgrey;
 	}
 	.send form .row .col-md-11, .send form .row .col-md-1 {
    padding: 0px;
}
 </style>
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="buddies">
				
			</div>
		</div>
		<div class="col-md-8">
			<div class="msg-header">
				<h3 class="text-center"></h3>
			</div>
			<div class="msg-body" style="overflow: scroll; height: 400px;">
				
			</div>
			<div class="send">
				<form action="" method="POST" id="msg-form">
					<div class="row">
						<div class="col-md-11">
							<input type="email" name="receiver" value="" style="display: none;">
							<textarea name="message" placeholder="Type a message" id="" cols="30" rows="2" class="form-control"></textarea>
						</div>
						<div class="col-md-1">
							<span><button class="btn btn-primary">Send</button></span>
						</div>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>
<?php require 'footer.php'; ?>
