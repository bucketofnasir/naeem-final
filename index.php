<?php require 'header.php'; 
$info=array();
if ($_POST) {
	$creator=$_POST['creator'];
	$name=$_POST['name'];
	$email=$_POST['email'];
	$phone=$_POST['phone'];
	$password=$_POST['password'];
	$gender=$_POST['gender'];
	$religion=$_POST['religion'];
	$birthdate=(string)$_POST['birthdate'];
	$sql="INSERT INTO users(id,creator,name,email,password,phone,dateofbirth,gender,religion) VALUES(NULL,'$creator','$name','$email','$password','$phone','$birthdate','$gender','$religion')";
	if (mysqli_query($con,$sql)) { 
		if ($gender=='Male') {
			$image='upload/default/groom-default.jpg';
		}elseif ($gender=='Female') {
			$image='upload/default/bride-default.jpg';
		}
		$xx=(string)$_POST['birthdate'];
		$age= date('Y')-substr($xx, 0,4);
		$sql="INSERT INTO basic_info(email, about, age, marital_status, diet, image, height) VALUES('$email','Not set','$age', 'Not set','Not set','$image','Not set')";
		if (mysqli_query($con,$sql)) {
			$partner_sql="INSERT INTO partner(email, min_age, max_age, min_height, max_height, partner_marital_status, partner_religion, partner_profession) VALUES('$email','Not set','Not set','Not set','Not set','Not set','Not set','Not set')";
			if (mysqli_query($con, $partner_sql)) {
			$contact_sql="INSERT INTO contact_info(email, contact_name, contact_number, relation, contact_time) VALUES('$email','Not set','Not set','Not set','Not set')";
				if (mysqli_query($con,$contact_sql)) {
					$education_sql="INSERT INTO educational_details(email, undergraduate, postgraduate, university, college, profession, company, designation, salary) VALUES('$email','Not set','Not set','Not set','Not set','Not set','Not set','Not set','Not set')";
					if (mysqli_query($con,$education_sql)) {
						$family_sql="INSERT INTO family_details(email, father_profession, mother_profession, brothers, sister, family_value, family_type, family_affluence, live) VALUES('$email','Not set','Not set','Not set','Not set','Not set','Not set','Not set','Not set')";
						if (mysqli_query($con,$family_sql)) {
							$family_sql="INSERT INTO lifestyle(email, weight, habits, mother_tongue, language, blood, disability) VALUES('$email','Not set','Not set','Not set','Not set','Not set','Not set')";
							if (mysqli_query($con,$family_sql)) {
								$info[]="Account is successfully created!";
							}
						}
					}
				}
			}
		}
	}else{ 
		$info[]="Account is not created";
	}
}

 ?>
<div class="w3layouts-banner" id="home">
<div class="container">
	<div class="clearfix"></div>
	<?php if (isset($_SESSION['name'])) { ?>

	

	<?php }else{ ?>
	
		<div class="jumbotron">
  <h4 class="display-4">Register NOW!</h4>
  <h4 class="lead"><?php foreach ($info as $key => $value) {
			echo $value;
		} ?></h4>
  <hr class="my-4">
  <p class="lead">
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
				<div class="form-group row">
					<label for="create_by" class="col-sm-2 col-form-label">Profile For:</label>
					<select id="create_by" name="creator" class="col-10 form-control required">
						<option value="">Select</option>
						<option value="myself">Myself</option>   
						<option value="parent">Son</option>   
						<option value="parent">Daughter</option>   
						<option value="brother">Brother</option>   
						<option value="brother">Sister</option>  
						<option value="relative">Relative</option>
						<option value="friend">Friend</option>						
					</select>
				</div>
				<div class="form-group row">
					<label for="name" class="col-2 col-form-label">Name:</label>
					<input type="text" class="col-10 form-control" name="name" placeholder="Full name" id="name" required=""/>
				</div>
				<div class="form-group row">
					<label for="gender" class="col-2 col-form-label">Gender:</label>
					<div class="col-2">
						<label class="radio"><input type="radio" name="gender" checked="" value="Male"><i></i>Male</label>
					</div>
					<div class="col">
						<label class="radio"><input type="radio" name="gender" value="Female"><i></i>Female</label>
					</div>
				</div>
				<div class="form-group row">
					<label for="date" class="col-2 col-form-label">Date Of Birth:</label>
					<input type="date" class="col-10 form-control" id="date" name="birthdate" required="" />
				</div>
				<div class="form-group row">
					<label for="religion" class="col-2 col-form-label">religion:</label>
					<select id="religion" name="religion" class="col-10 form-control"> 
						<option value="">Select Religion</option>
						<option value="Muslim">Muslim</option>
						<option value="Hindu">Hindu</option>    
						<option value="Christian">Christian</option>   
						<option value="Sikh">Sikh</option>   
						<option value="Jain">Jain</option>   
						<option value="Buddhist">Buddhist</option>
						<option value="Secular">No Religious Belief</option>   						
					</select>
				</div>
				<div class="form-group row">
				<label for="phone" class="col-2 col-form-label">Mobile No:</label>
					<input id="phone" placeholder="+88" class="col-10 form-control" name="phone" type="tel">
				</div>
				<div class="form-group row">
					<label for="email" class="col-2 col-form-label">Email:</label>
					<input type="email" id="email" class="col-10 form-control" name="email" placeholder="Type an email" required=""/>
				</div>
				<div class="form-group row">
					<label for="password" class="col-2 col-form-label">Password:</label>
					<input type="password" id="password" class="col-10 form-control" name="password" placeholder="Type password" required=""/>
				</div>
				
				<input type="submit" class="btn btn-primary" value="Register me" />
				<div class="clearfix"></div>
				<p class="w3ls-login">Already a member? <a href="login.php" >Login</a></p>
			</form>
  </p>
<?php } ?>
		
</div>

			   
	
<?php require 'footer.php'; ?>
<script>
	$(document).ready(function() {
		$('.home').addClass('active');
	});
</script>