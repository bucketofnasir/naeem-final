<?php 
require '../connection.php';
session_start();
$email=$_SESSION['email'];
$undergraduate=$_POST['undergraduate'];
$postgraduate=$_POST['postgraduate'];
$university=$_POST['university'];
$college=$_POST['college'];
$profession=$_POST['profession'];
$company=$_POST['company'];
$designation=$_POST['designation'];
$salary=$_POST['salary'];

$basic_sql="UPDATE educational_details SET undergraduate='$undergraduate', postgraduate='$postgraduate', university='$university', college='$college', profession='$profession', company='$company', designation='$designation', salary='$salary' WHERE email='$email'";
if (mysqli_query($con,$basic_sql)) {
	$_SESSION['msg']= '<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>Success! </strong>Education and career is updated is updated successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
		header('location: ../editprofile.php?update=education');
}else{
	$_SESSION['msg']= '<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <strong>Fail! </strong>Education and career is not updated<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
		header('location: ../editprofile.php?update=education');
}