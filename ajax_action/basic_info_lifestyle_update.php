<?php 
require '../connection.php';
session_start();
$email=$_SESSION['email'];
$about=$_POST['about'];
$marital_status=$_POST['marital_status'];
$diet=$_POST['diet'];
$height=$_POST['height'];
$weight=$_POST['weight'];
$habits=$_POST['habits'];
$mother_tongue=$_POST['mother_tongue'];
$languages=$_POST['languages'];
$blood=$_POST['blood'];
$disability=$_POST['disability'];
$hair=$_POST['hair'];
$skin=$_POST['skin'];
$eye=$_POST['eye'];
$freckle=$_POST['freckle'];
$dimple=$_POST['dimple'];

$basic_sql="UPDATE basic_info SET about='$about', marital_status='$marital_status', diet='$diet', height='$height', skin='$skin', hair_color='$hair', eye_color='$eye', dimple='$dimple', freckle='$freckle' WHERE email='$email'";
if (mysqli_query($con,$basic_sql)) {
	$lifestyle_sql="UPDATE lifestyle SET weight='$weight', habits='$habits', mother_tongue='$mother_tongue', language='$languages', blood='$blood', disability='$disability' WHERE email='$email'";
	if (mysqli_query($con,$lifestyle_sql)) {
		$_SESSION['msg']= '<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>Success! </strong>Basic info and lifestyle is updated successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
		header('location: ../editprofile.php?update=basic');
	}else{
		$_SESSION['msg']= '<div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong>Warning! </strong>Basic info is updaed but lifestyle is not updated<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
		header('location: ../editprofile.php?update=basic');
	}
}else{
	$_SESSION['msg']= '<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <strong>Fail! </strong>Basic info & lifestyle is not updated<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
	header('location: ../editprofile.php?update=basic');
}