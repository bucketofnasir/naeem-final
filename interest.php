<?php require 'header.php'; 
if (isset($_SESSION['email'])) {
	$email=$_SESSION['email'];
}else{
	header('location:login.php');
}
?>
<div class="container">
	<div class="row">
		<div class="col col-md-4 col-12">
			<ul class="list-group">
			  <li class="list-group-item"><a href="interest.php?type=receive">View receive interest</a></li>
			  <li class="list-group-item"><a href="interest.php?type=sent">View sent interest</a></li>
			</ul>
		</div>
		<div class="col col-md-8 col-12">
			<?php if (isset($_GET['type'])) {
				$type=$_GET['type'];
				if ($type=='receive') {
					if (isset($_SESSION['rec'])) {
						echo $_SESSION['rec'];
						unset($_SESSION['rec']);
					}
					$sql="SELECT * FROM interest WHERE receiver='$email' AND status=0";
					$rst=mysqli_query($con, $sql);
					if ($rst->num_rows) { ?>
						<table class="table table-stripe">
							<thead>
								<tr>
									<td>Sender</td>
									<td>Action</td>
								</tr>
							</thead>
							<tbody>
					<?php while ($row=mysqli_fetch_assoc($rst)) { 
							$sender=$row['sender'];
							$s="SELECT * FROM users WHERE email='$sender'";
							if ($r=mysqli_query($con, $s)) {
								while ($rr=mysqli_fetch_assoc($r)) { ?>
								<tr>
									<td><a href="groom_profile.php?id=<?php echo($rr['id']) ?>"><?php echo $rr['name']; ?></a></td>
									<td><a href="interest.php?type=accept&email=<?php echo($rr['email']); ?>">Accept</a></td>
								</tr>
							<?php }
							}
						} ?>
							</tbody>
						</table>
					<?php }else{
						echo '<h1 class="container">No interest found</h1>';
					}
				}elseif ($type=='sent') {


					$sql="SELECT * FROM interest WHERE sender='$email' AND status=0";
					$rst=mysqli_query($con, $sql);
					if ($rst->num_rows) { ?>
						<table class="table table-stripe">
							<thead>
								<tr>
									<td>Receiver</td>
									<td>Action</td>
								</tr>
							</thead>
							<tbody>
					<?php while ($row=mysqli_fetch_assoc($rst)) { 
							$receiver=$row['receiver'];
							$s="SELECT * FROM users WHERE email='$receiver'";
							if ($r=mysqli_query($con, $s)) {
								while ($rr=mysqli_fetch_assoc($r)) { ?>
								<tr>
									<td><a href="groom_profile.php?id=<?php echo($rr['id']) ?>"><?php echo $rr['name']; ?></a></td>
									<td><a href="">Deleted</a></td>
								</tr>
							<?php }
							}
						} ?>
							</tbody>
						</table>
					<?php }else{
						echo '<h1 class="container">No interest found</h1>';
					}


					
				}elseif ($type=='accept') {
					$e=$_GET['email'];
					$acc="UPDATE interest SET status=1 WHERE sender='$e' AND receiver='$email'";
					if (mysqli_query($con, $acc)) {
						$_SESSION['rec']='<p class="text-success">Interest accepted</p>'; ?>
						<script>location.href="interest.php?type=receive"</script>						
				<?php }
				}
			}else{

			} ?>
		</div>
	</div>
</div>
<?php require 'footer.php'; ?>