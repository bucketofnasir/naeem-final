<?php 
/**
* 
*/
class allsidebar
{
	
	function __construct(){}

	function result_sidebar(){ ?>
	<div class="col-md-3 w3ls-aside">
			<h4>Filter Profiles by</h4>
			<div class="fltr-w3ls">
				<h5>City</h5>
				<select class="form-control">
					<option><a href="#">Dhaka</a></option>
					<option><a href="#">Comilla</a></option>
					<option><a href="#">Chittagong</a></option>
					<option><a href="#">Rajshahi</a></option>
					<option><a href="#">Rangpur</a></option>
					<option><a href="#">Sylhet</a></option>
					<option><a href="#">Barishal</a></option>
					<option><a href="">Khulna</a></option>
				</select>
			</div>
			<div class="fltr-w3ls">
				<h5>Religion</h5>
				<select class="form-control">
					<option><a href="#">Islam</a></option>
					<option><a href="#">Hindu</a></option>
					<option><a href="#">Christian</a></option>
					<option><a href="#">Jews</a></option>
					<option><a href="#">Catholic</a></option>
					<option><a href="#">Buddhist</a></option>
				</select>
			</div>
		</div>
	<?php }
	function breadcrumbs($ar){ ?>
	<!-- breadcrumbs -->
	<div class="w3layouts-breadcrumbs text-center">
		<div class="container">
			<span class="agile-breadcrumbs">
				<?php
				$c=count($ar);
				 for ($i=0; $i<=$c; $i++) { 
					if ($i==$c) {
						echo "<span>Looking for ".$_GET['gender']. " </span>";
					}else{
						echo '<a href="'.$ar[$i].'.php">'.$ar[$i].'</a> >'; 
					}
					?>
				<?php } ?>
			</span>
		</div>
	</div>
	<!-- //breadcrumbs -->
	<?php }
}