<?php require 'header.php';
$info = array();
if ($_POST) {
	$email=$_POST['login_email'];
	$pass=$_POST['login_password'];
	if ($email==""||$pass=="") {
		if ($email=="") {
			$info[]= '<div class="alert alert-warning alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  <strong>Warning! </strong>Email is required</div>';
		}
		if ($pass=="") {
			$info[]= '<div class="alert alert-warning alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  <strong>Warning! </strong>Password is required</div>';
		}
	}else{
		$sql="SELECT * FROM users WHERE email='$email' AND password='$pass'";
		$date=date('Y-m-d H:i:s');
		$result=mysqli_query($con,$sql);
		if ($result->num_rows) {
			$s="INSERT INTO session(email, session_key, session_start) VALUES('$email', '$email', '$date')";
			if (mysqli_query($con, $s)) {
				while ($row=mysqli_fetch_assoc($result)) {
					$_SESSION['email']=$row['email'];
					$_SESSION['name']=$row['name'];
					header('location: index.php');
				}
			}
		}else{
			$info[]= '<div class="alert alert-danger alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  <strong>Error!</strong>Your email or password is error</div>';
		}
	}

} ?>
<div class="card border-primary mb-3" style="margin: 0 auto;max-width: 20rem;">
  <div class="card-header">Login</div>
  <div class="card-body text-primary">
    <h4 class="card-title"><?php foreach ($info as $key => $value) {
    	echo $value;
    } ?></h4>
    <form action="<?php echo($_SERVER['PHP_SELF']); ?>" id="login-page" method="POST">
		<div id="signin">
			<div class="form-group email_group">
				<label class="control-label" for="login_email">Email </label>
				<input type="email" class="form-control" name="login_email" placeholder="Email" id="login_email">
			</div>
			<div class="form-group password_group">
				<label class="control-label" class="control-label" for="login_password">Password</label>
				<input type="password" name="login_password" placeholder="Password" class="form-control" id="login_password">
			</div>	
			
			<button class="btn btn-success">Login</button>
			
		</div>
	</form>
  </div>
</div>	
<?php require 'footer.php'; ?>