<?php
require 'header.php'; 
if (!isset($_SESSION['email'])) { ?>
	<script>window.location='index.php';</script>
<?php }
$email=$_SESSION['email'];
?>
<style>
	div.page-wrapper{
	background: #f1f1f2!important;
}
.alert{
	font-size: 1.85em;
}

</style>

<div class="page-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
				  <div class="panel-body">
				    <div>
  <div class="">
    <div id="home" class="">
      <h3>Upload profile picture</h3>
      <div class="imgupinfo">

      	<?php if (isset($_SESSION['upload'])) {
      		echo $_SESSION['upload'];
      		unset($_SESSION['upload']);
      	} ?>
      </div>
      <form action="upload.php" method="POST" enctype="multipart/form-data">
      	<div class="form-group">
      		<?php $sql="SELECT * FROM basic_info WHERE email='$email'";
      		if ($rst=mysqli_query($con, $sql)) {
      			while ($r=mysqli_fetch_assoc($rst)) { ?>
      				<img src="<?php echo($r['image']); ?>" alt="" style="height: 200px;width: 200px;">
      				<input type="file" name="image" value="<?php echo $r['image']; ?>" class="form-control" required>
      			<?php }
      		 } ?>
      	</div>
      <button class="btn btn-primary">upload</button>
      </form>
      <form action="ajax_action/update_profile.php" id="profile" method="POST">
        <div class="panel-body">
		  	<div class="pr_msg"></div>
		  	<?php $sql="SELECT * FROM users WHERE email='$email'";
		  	if ($result=mysqli_query($con,$sql)) {
		  		while ($row=mysqli_fetch_assoc($result)) { ?>
		  		
		  		<div class="form-group creator_group">
		  			<label class="control-label" for="creator">Created for*</label>
		  			<select name="creator" id="creator" class="form-control">
		  				<option value="<?php echo($row['creator']); ?>"><?php echo($row['creator']); ?></option>
		  				<option value="">Select Creator</option>
						<option value="myself">Myself</option>   
						<option value="parent">Son</option>   
						<option value="parent">Daughter</option>   
						<option value="brother">Brother</option>   
						<option value="brother">Sister</option>  
						<option value="relative">Relative</option>
						<option value="friend">Friend</option>
					</select>
		  		</div>
		  		<div class="form-group name_group">
		  			<label class="control-label" for="name">Name*</label>
		  			<input type="text" name="name" id="name" class="form-control" value="<?php echo($row['name']); ?>">
		  		</div>


		  		<div class="form-group email_group">
		  			<label class="control-label" for="email">Email*</label>
		  			<input type="email" name="email" id="email" class="form-control" value="<?php echo($row['email']); ?>" disable>
		  		</div>

		  		<div class="form-group birthdate_group">
		  			<label class="control-label" for="birthdate">Date of birth</label>
		  			<input type="text" name="birthdate" id="birthdate" placeholder="MM/DD/YYYY" class="form-control" value="<?php echo($row['dateofbirth']); ?>">
		  		</div>


		  		<div class="form-group phone_group">
		  			<label class="control-label" for="phone">Phone</label>
		  			<input type="text" name="phone" id="phone" class="form-control" value="<?php echo($row['phone']); ?>">
		  		</div>


		  		<div class="form-group">
		  			<button class="btn btn-primary" onclick="saveProfile()">Save Changes</button>
		  		</div>
		  	
		  	<?php	}
		  	 } ?>
		  </div>
		</form>
    </div>
    
  </div>

</div>
				  </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require 'footer.php'; ?>
