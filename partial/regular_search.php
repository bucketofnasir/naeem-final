<div class="card">
  <h4 class="card-header">Search for your partner</h4>
  <div class="card-body">


<form action="result.php" method="GET">	
   <div class="form-group">
	<div class="row">
		<label class="col-3" for="sex">Gender : </label>
		<div class="col-9 form_radios">
			<input type="radio" id="male" name="gender" value="male" checked> <label for="male">Male</label>
			<input type="radio" id="female" name="gender" value="female"> <label for="female">Female</label>
		</div>
	</div>
</div>
<div class="form-group">
	<div class="row">
		<label class="col-3" for="sex">Age in years : </label>
	<div class="col-9">
		<select id="ageFrom" name="age_from" onchange="from_age()" class="frm-field required">
			<option value="">--------</option><option value="18">18</option><option value="19">19</option>  <option value="20">20</option><option value="21">21</option><option value="22">22</option>   
			<option value="23">23</option><option value="24">24</option><option value="25">25</option>  	
			<option value="26">26</option><option value="27">27</option><option value="28">28</option>  
			<option value="29">29</option><option value="30">30</option><option value="31">31</option>  
			<option value="32">32</option><option value="33">33</option><option value="34">34</option>  
			<option value="35">35</option><option value="36">36</option><option value="37">37</option> 
			<option value="38">38</option><option value="39">39</option><option value="40">40</option>
		</select>
		<span>To </span>
		<select id="age_to" name="age_to" class="frm-field required">
		</select>
	</div>
	</div>
</div>
<div class="form-group">
	<div class="row">
		<label class="col-3" for="sex">Height : </label>
	<div class="col-9">
		<select id="heightFrom" name="height_from" onchange="height_From()" class="frm-field required">
			<option value="0">--------</option>
			<option value="4.0">4ft</option><option value="4.1">4ft 1in</option><option value="4.2">4ft 2in</option><option value="4.3">4ft 3in</option><option value="4.4">4ft 4in</option><option value="4.5">4ft 5in</option><option value="4.6">4ft 6in</option><option value="4.7">4ft 7in</option><option value="4.8">4ft 8in</option><option value="4.9">4ft 9in</option><option value="5.0">5ft</option>
		</select>
		<span>To </span>
		<select id="heightTo" name="height_to" class="frm-field required">
								
		</select>
	</div>
	</div>
</div>				
  <div class="form-group">
	<div class="row">
		<label class="col-3" for="sex">Marital Status : </label>
	<div class="col-9">
		<input type="checkbox" class="radio_1"> Single &nbsp;&nbsp;
		<input type="checkbox" class="radio_1" checked="checked"> Divorced &nbsp;&nbsp;
		<input type="checkbox" class="radio_1" value="Cheese"> Widowed &nbsp;&nbsp;
		<input type="checkbox" class="radio_1" value="Cheese"> Separated &nbsp;&nbsp;
		<input type="checkbox" class="radio_1" value="Cheese"> Any
	</div>
	</div>
  </div>
  <div class="form-group">
	<div class="row">
		<label class="col-3" for="city">District / City : </label>
	<div class="col-9">
	  <div class="select-block1">
		<select name="city">
			<option value="">District / City</option>
			<option value="Dhaka">Dhaka</option>
			<option value="Chittagong">Chittagong</option>
			<option value="Comilla">Comilla</option>
			<option value="Rajshahi">Rajshahi</option>
			<option value="Rangpur">Rangpur</option> 
			<option value="Khulna">Khulna</option> 
			<option value="Shylet">Shylet</option> 
			<option value="Barisal">Barisal</option>
		</select>
	  </div>
	</div>
	</div>
  </div>
  <div class="form-group">
	<div class="row">
		<label class="col-3" for="religion">Religion : </label>
	<div class="col-9">
	  <div class="select-block1">
		<select name="religion">
			<option value="Muslim">Muslim</option> 
			<option value="Hindu">Hindu</option>
			<option value="Sikh">Sikh</option>
			<option value="Jain">Jain</option>
			<option value="Christian">Christian</option>  
			<option value="Jewish">Jewish</option>
			<option value="Bhuddhist">Bhuddhist</option> 
		</select>
	  </div>
	</div>
	</div>
  </div>
  <div class="form-group">
	<div class="row">
		<label class="col-3" for="sex">Mother Tongue : </label>
	<div class="col-9">
	  <div class="select-block1">
		<select name="language">
			<option value="Bangla">Bangla</option>
			<option value="English">English</option>
			<option value="Hindi">Hindi</option>
			<option value="Urdu">Urdu</option>
		</select>
	  </div>
	</div>
	</div>
  </div>
  
  <input type="submit" value="Search" class="btn btn-primary" />
</form>
</div>  </div>
<script type="text/javascript">
	function from_age(){
    	var xmlHttp=new XMLHttpRequest(); //class
	    xmlHttp.open("GET","ajax_action/fetch_age_to.php?from="+document.getElementById('ageFrom').value, false);
	    xmlHttp.send(null);// default function
	    document.getElementById('age_to').innerHTML=xmlHttp.responseText;
  }

  function height_From(){
    	var xmlHttp=new XMLHttpRequest();
	    xmlHttp.open("GET","ajax_action/fetch_height_to.php?from="+document.getElementById('heightFrom').value, false);
	    xmlHttp.send(null);
	    document.getElementById('heightTo').innerHTML=xmlHttp.responseText;//for print
  }
</script>