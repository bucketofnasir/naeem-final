<?php $con=new mysqli('127.0.0.1','root','','naeem'); session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Welcome to our matrimonial site</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
</head>
<body class="container-fluid">
<header>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php">Matrimonial</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="search.php">Search</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Quick search
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <form action="result.php" method="GET">
          	<div class="form-group">
          		<label for="gender">I am looking for: </label>
          		<select id="gender" name="gender" class="form-control">
          			<option value="Female">Bride</option>
          			<option value="Male">Groom</option>
          		</select>
          	</div>
          	<div class="form-group">
          		<label>Age:</label>
          		<div class="row">
          			<div class="col">
          				<select name="age_from" class="form-control">
          					<option value="18">18</option>   
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>   
							<option value="22">22</option>   
							<option value="23">23</option>   
							<option value="24">24</option>   
							<option value="25">25</option>  
          				</select>
          			</div>
          			<div class="col">
          				<select name="age_to" class="form-control">
          					<option value="20">20</option>
							<option value="21">21</option>   
							<option value="22">22</option>   
							<option value="23">23</option>   
							<option value="24">24</option>
							<option value="25">25</option>
							<option value="26">26</option>   
							<option value="27">27</option>   
							<option value="28">28</option>   
							<option value="29">29</option>
							<option value="30">30</option>
							<option value="31">31</option>   
							<option value="32">32</option>   
							<option value="33">33</option>   
							<option value="34">34</option>   
							<option value="35">35</option>  
							<option value="">- - -</option>   					 
          				</select>
          			</div>
          		</div>
          	</div>
          	<div class="form-group">
          		<label for="religion">Select religion</label>
          		<select id="religion" class="form-control" name="religion">
          			<option value="muslim">Muslim</option>  
					<option value="hindu">Hindu</option>   
					<option value="christian">Christian</option>   
					<option value="sikh">Sikh</option>   
					<option value="jain">Jain</option>   
					<option value="buddhist">Buddhist</option>
					<option value="">No Religious Belief</option>   	
          		</select>
          	</div>
          	<button class="btn btn-primary">search</button>
          </form>
        </div>
      </li>
      
    </ul>
    <span class="navbar-text">
	<ul class="navbar-nav mr-auto">
		<?php if (isset($_SESSION['email'])) { 
			$email=$_SESSION['email']; ?>

		<li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          <?php 
	          $i="SELECT * FROM basic_info WHERE email='$email'";
				if ($r=mysqli_query($con, $i)) {
				 	while ($rw=mysqli_fetch_assoc($r)) { ?>
				 	<img src="<?php echo($rw['image']); ?>" alt="" style="transform: scale(1.75);height: 15px;border-radius: 50%;">
				 <?php	}
				  }
	           ?>
	           <?php echo $_SESSION['name']; ?>
	        </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="profile.php">My profile</a>
	          <a class="dropdown-item" href="settings.php">Account settings</a>
	          <div class="dropdown-divider"></div>
	          <a class="dropdown-item" href="logout.php">Log out</a>
	        </div>
	      </li>
	      <li class="nav-item">
			<a class="nav-link" href="inbox.php">Inbox</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="interest.php">Interest</a>
			</li>
		<?php }else{ ?>
		<li class="nav-item">
			<a class="nav-link" href="login.php">Login</a>
		</li>
		<?php } ?>

		
	</ul>
    </span>
  </div>
</nav>
</header>